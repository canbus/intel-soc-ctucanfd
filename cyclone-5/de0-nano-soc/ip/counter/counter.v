
`timescale 1 ps / 1 ps
module counter (
		input  wire       clk,
		output reg [63:0] counter,
		output wire       indication,
		input  wire       rst
	);

	assign indication = counter[25];

	always @(posedge clk) begin
		if (!rst) begin
			counter <= {64{1'b0}};
		end else begin
			counter <= counter + 1;
		end
	end

endmodule
